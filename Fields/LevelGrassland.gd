class_name LevelGrassland
extends Node2D

@onready var packed_player = preload("res://Player/Player.tscn")
@onready var packed_bat = preload("res://Enemies/Bat/Mob_Bat.tscn")
@onready var packed_Spawner = preload("res://Enemies/Bat/MobBatSpawner.tscn")
@onready var player: Player = null
@onready var bgm_player: AudioStreamPlayer = $BgmPlayer


#
# ワールドの初期化
#
func _ready():
	player = packed_player.instantiate()
	player.position = Vector2(0, 0)
	bgm_player.play()
	add_child(player)
	GameManager.set_player(player)
	player.activate_weapons()

	var connect_error = player.status_changed.connect(_on_player_status_changed)
	assert(connect_error == OK, "connect error: player signal('status_changed').")
	var spawner = packed_Spawner.instantiate()
	add_child(spawner)

	$HUD.start()


#
# [event] プレイヤーの状態が変化した時
#
func _on_player_status_changed(status):
	match status:
		player.Status.ALIVE:
			pass
		player.Status.DEATH:
			bgm_player.stop()
			var dead_music: Resource = load("res://Music/力及ばず.ogg")
			bgm_player.stream = dead_music
			bgm_player.play()
			if player.status_changed.is_connected(_on_player_status_changed):
				player.status_changed.disconnect(_on_player_status_changed)
			show_game_over_dialog()


#
# ゲームオーバー ダイアログの表示
#
func show_game_over_dialog():
	# 画面サイズを取得
	var screen_size: Vector2 = get_viewport_rect().size
	# 画面中央に表示
	var pos_x = (screen_size.x - $GameOverDialog.width) / 2
	var pos_y = (screen_size.y - $GameOverDialog.height) / 2
	$GameOverDialog.transform.origin  = Vector2(pos_x, pos_y)
	$GameOverDialog/CenterContainer.visible = true
