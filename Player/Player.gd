class_name Player
extends CharacterBody2D

#
# exported variables
#
@export var speed: float = 100.0
@export var hit_point: float = 100.0
@export var max_hit_point: float = 100.0

#
# Node objects
#
@onready var hit_point_bar: ProgressBar = $HpBar
@onready var animated_sprite: AnimatedSprite2D = $AnimatedSprite2D
@onready var animation_player: AnimationPlayer = $AnimationPlayer

@onready var packed_level_up_dialog = preload("res://Player/PlayerLevelUpDialog.tscn")

#
# Class fields
#
var is_death: bool = false
# レベルアップ テーブル
#                                          1,  2,   3,   4,   5,   6,    7
@export var level_up_table: Array = [0, 50, 100, 200, 400, 800, 1600]
# レベルキャップ
@onready var level_cap: int =  level_up_table.size()
var player_level: int = 0
# 総経験値
var total_xp_point: float = 0.0: set = set_total_xp_point

#
# Signals
#
signal hp_changed
signal position_changed
signal status_changed
signal xp_point_changed
signal level_changed

enum Status {IDLE, ALIVE, DEATH = -1}

#
# [property] 総経験値を設定する
#
func set_total_xp_point(new_valie: float):
	total_xp_point = new_valie


#
# [event] 初期化
#
func _ready() -> void:
	init_player_value()
	hp_changed.emit(hit_point, max_hit_point)
	position_changed.emit(position)
	status_changed.emit(Status.ALIVE)


#
# Init player variables
#
func init_player_value() -> void:
	hit_point_bar.max_value = max_hit_point
	is_death = false


#
# [event] 物理プロセス
#
func _physics_process(_delta):
	if is_death:
		pass
	else:
		velocity = get_input()
		move_player(velocity)

	change_hp_point_bar(hit_point)
	hp_changed.emit(hit_point, max_hit_point)
	position_changed.emit(position)


#
# プレイヤーの移動
#
func move_player(new_velocity: Vector2):
	set_velocity(new_velocity)
	move_and_slide()


#
# HPバーの更新
#
func change_hp_point_bar(new_hit_point):
	hit_point_bar.value = new_hit_point


#
# 入力チェックをチェックし、速度ベクとトル(velocity) を返す。
#
func get_input() -> Vector2:
	var vel: Vector2 = Vector2()
	var rd = Input.is_action_pressed("ui_right") and Input.is_action_pressed("ui_down")
	var ru = Input.is_action_pressed("ui_right") and Input.is_action_pressed("ui_up")

	var ld  = Input.is_action_pressed("ui_left") and Input.is_action_pressed("ui_down")
	var lu  = Input.is_action_pressed("ui_left") and Input.is_action_pressed("ui_up")

	var r  = Input.is_action_pressed("ui_right")
	var u  = Input.is_action_pressed("ui_up")

	var l   = Input.is_action_pressed("ui_left")
	var d   = Input.is_action_pressed("ui_down")
	
	animated_sprite.play()
	if rd:
		# 右下
		vel.x += 1
		vel.y += 1
		animated_sprite.animation = "right"
	elif ru:
		# 右上
		vel.x += 1
		vel.y -= 1
		animated_sprite.animation = "right"
	elif ld:
		# 左下
		vel.x -= 1
		vel.y += 1
		animated_sprite.animation = "left"
	elif lu:
		# 左上
		vel.x -= 1
		vel.y -= 1
		animated_sprite.animation = "left"
	elif l:
		# 左
		vel.x -= 1
		animated_sprite.animation = "left"
	elif r:
		# 右
		vel.x += 1
		animated_sprite.animation = "right"
	elif d:
		# 下
		vel.y += 1
		animated_sprite.animation = "front"
	elif u:
		# 上
		vel.y -= 1
		animated_sprite.animation = "back"
	else:
		animated_sprite.stop()

	vel = vel.normalized() * speed

	return vel


#
# [ebent] 敵から攻撃された時
# damage: 受けたダメージ量
#
func attacked_from_enemy(damage: float):
	if is_death:
		# 既に死んでいる場合
		pass
	else:
		animation_player.play("damaged")
		hit_point = max(hit_point - damage, 0.0)
		if hit_point <= 0.0:
			# 死亡処理
			dead()
		else:
			pass

		hp_changed.emit(hit_point, max_hit_point)


#
# 死亡処理
#
func dead():
	is_death = true
	animated_sprite.animation = "death"
	animated_sprite.offset.y = -6
	status_changed.emit(Status.DEATH)


#
# 武器を装備する
#
func activate_weapons():
	$ThrowingKnifeLauncher.create_weapons()


#
# レベルアップ処理
#
func level_up():
	player_level += 1
	level_changed.emit(player_level)
	# ToDo: レベルアップ画面を表示
	show_level_up_dialog(player_level)
	print("level: %d" % [player_level])


#
# XP point を加算
#
func increment_xp_point(new_xp_point: float) -> void:
	total_xp_point = total_xp_point + new_xp_point
	while true:
		if level_cap <= player_level:
			# レベルキャップに達した場合は、レベルアップなし
			break
		if level_up_table[player_level] < total_xp_point:
			level_up()
		else:
			break
	xp_point_changed.emit(total_xp_point)

#
# レベルアップ ダイアログの表示
#
func show_level_up_dialog(new_level: int):
	var level_up_dialog = packed_level_up_dialog.instantiate()
	add_child(level_up_dialog)

	var screen_size: Vector2 = get_viewport_rect().size
	var pos_x = (screen_size.x - level_up_dialog.width) / 2
	var pos_y = (screen_size.y - level_up_dialog.height) / 2
	level_up_dialog.transform.origin  = Vector2(pos_x, pos_y)
	level_up_dialog.show_dialog(new_level)
