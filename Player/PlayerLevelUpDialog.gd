extends CanvasLayer

# ダイアログを閉じたあと、一時停止を解除するまでの時間
@export var restart_wait_timer: float = 0.5

var width: float = 0.0
var height: float = 0.0
var selected_item: int = 0

#
# 初期化
#
func _ready():
	width = $Popup.size.x
	height = $Popup.size.y


#
# レベルアップ ダイアログを表示する
# 1.ゲームを停止
# 2.先頭のアイテムにフォーカスを設定
# 3.レベルを表示
#
func show_dialog(new_level: int):
	get_tree().paused = true
	$Popup/ItemList.select(0, false)
	$Popup/ItemList.grab_focus()
	$Popup/Label.text = "レベルアップ！ Lv%d" % [new_level]
	$Popup.show()


#
# [event] アイテム選択時
#
func _on_ItemList_item_selected(index):
	selected_item = index


#
# [event] アイテム確定時
#
func _on_ItemList_item_activated(_index):
	# ToDo: アイテム確定時の処理を追加
	print("%d selected" % [selected_item])
	await get_tree().create_timer(restart_wait_timer).timeout
	get_tree().paused = false
	queue_free()
