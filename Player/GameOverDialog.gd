extends CanvasLayer

var width: float = 0.0
var height: float = 0.0

func _ready():
	width = $CenterContainer.size.x
	height = $CenterContainer.size.y
