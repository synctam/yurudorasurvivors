extends Area2D

var xp_point: float = 0.0: set = set_xp_point

func _ready():
	pass

func set_xp_point(new_value: float) -> void:
	xp_point = new_value


#
# [event] プレイヤーと接触した時、プレイヤーの xp_point を更新する。
#
func _on_xp_Jewel_body_entered(body):
	if body is Player:
		# 取得アニメーションを表示
		# Tween は二段階に分けて実行する。
		# プレイヤーに向かってGEMを移動。
		var tween = get_tree().create_tween()
		# プレイヤーに向かって移動
		tween.tween_property(self,"position", GameManager.player.position, 0.3)
		tween.set_trans(Tween.TRANS_EXPO)
		tween.set_ease(Tween.EASE_IN_OUT)
		# アニメーション終了時のコールバックを登録
		tween.tween_callback(_on_tween_ended1)
		tween.set_loops() # 永久実行
		tween.play()

#
# [event] Tween 終了時
#
func _on_tween_ended1():
	GameManager.player.increment_xp_point(xp_point)
	queue_free()
