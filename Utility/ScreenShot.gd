class_name ScreenShot
extends Node2D

# ビューポートのパス
# 未指定の場合はルートを使います
@export var viewport_path: NodePath
# スクリーンショット機能の有無
@export var enabled: bool = true

# キャプチャするビューポート
@onready var target_viewport = get_node(viewport_path) if viewport_path else get_tree().root.get_viewport()


# PNGファイルとして保存する
func save_to(path: String = "") -> int:
	if enabled:
		var time : Dictionary = Time.get_datetime_dict_from_system();
		var display_datetime_string : String = "%d.%02d.%02d %02d.%02d.%02d" % [time.year, time.month, time.day, time.hour, time.minute, time.second];
		if path == "":
			path = "res://yurudora_%s.png" % display_datetime_string
		var img = target_viewport.get_texture().get_data()
		img.flip_y()
		var rc: int = img.save_png(path)
		print("The screenshot has been saved.")
		return rc
	else:
		return 0
