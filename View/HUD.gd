extends CanvasLayer

@onready var hp_bar = $PanelContainer/VBoxContainer2/HitPointBar
@onready var hp_label:Label = $PanelContainer/VBoxContainer2/LabelHP
@onready var xp_point_label = $PanelContainer/VBoxContainer2/LabelXpPoint

func _ready():
	var connect_error = GameManager.player_ready_changed.connect(_on_player_ready_changed)
	assert(connect_error == OK, "connect error: GameManager signal('player_ready_changed').")

func _on_player_ready_changed(is_player_ready_changed: bool):
	if is_player_ready_changed:
		var connect_error = GameManager.player.xp_point_changed.connect(_on_xp_point_changed)
		assert(connect_error == OK, "connect error: player signal('xp_point_changed').")
		xp_point_label.text = "Total XP: %.0f" % [0.0]


func start():
	var player = GameManager.player
	var connect_error = player.hp_changed.connect(_on_hp_changed)
	assert(connect_error == OK, "connect error: player signal('hp_changed').")

func _on_hp_changed(hp: float, max_hp: float):
	hp_label.text = "HP={0}/{1}".format([hp, max_hp])
	hp_bar.value = hp
	hp_bar.max_value = max_hp

#
# [event] XP が変化した時
#
func _on_xp_point_changed(new_xp_point: float) -> void:
	xp_point_label.text = "Total XP: %.0f" % [new_xp_point]
