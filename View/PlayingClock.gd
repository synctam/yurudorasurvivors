class_name PlayingClock
extends CanvasLayer

var is_player_dead: bool = false

# ゲーム開始時からの経過時間（秒）
var time: float = 0.0

func _ready():
	var connect_error = GameManager.player_ready_changed.connect(_on_player_ready_changed)
	assert(connect_error == OK, "connect error: player signal('status_changed').")

#
# [event] プレイヤーの準備できた時
#
func _on_player_ready_changed(is_ready: bool):
	if is_ready:
		var connect_error = GameManager.player.status_changed.connect(_on_player_status_changed)
		assert(connect_error == OK, "connect error: player signal('status_changed').")
	

func _process(delta: float) -> void:
	if not is_player_dead:
		time += delta
		var seconds = fmod(time,60)
		var minutes = fmod(time, 3600) / 60
		var str_elapsed = "%02d : %02d" % [minutes, seconds]
		$PlayingClockLabel.text = str_elapsed


#
# [event] プレイヤーの状態が変化した時
#
func _on_player_status_changed(state: int) -> void:
	match state:
		Player.Status.IDLE:
			is_player_dead = false
		Player.Status.ALIVE:
			is_player_dead = false
		Player.Status.DEATH:
			# 死亡時は攻撃タイマーを停止する
			is_player_dead = true
