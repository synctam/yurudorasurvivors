class_name MainMenu
extends Node2D

@onready var start_button: Button = $VBoxContainer/StartButton

func _ready():
	start_button.grab_focus()



func _on_StartButton_pressed():
	var scenr_chamge_error = get_tree().change_scene_to_file("res://Fields/LevelGrassland.tscn")
	assert(scenr_chamge_error == OK, "change_scene_to_file error: 'res://Fields/LevelGrassland.tscn'")

func _on_QuitButton_pressed():
	get_tree().quit()
