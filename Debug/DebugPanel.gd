extends CanvasLayer

@onready var player_pos: Label = $PanelContainer/VBoxContainer/PlayerPos
@onready var player: Player = null
@onready var fps_label:Label = $PanelContainer/VBoxContainer/FpsLabel
var ss_packed_scene: PackedScene = preload("res://Utility/ScreenShot.tscn")
var ss_object: ScreenShot = null 

# [debug] パフォーマンスチェック用SS
var ss_complated: bool = false;

func _ready() -> void:
	var connect_error = GameManager.player_ready_changed.connect(_on_player_ready_changed)
	assert(connect_error == OK, "connect error: GameManager signal('player_ready_changed')")
	ss_object = ss_packed_scene.instantiate()
	add_child(ss_object)

func _process(_delta):
	var fps = Engine.get_frames_per_second()
	fps_label.text = "FPS: %4.1f" % [fps]
	# [debug] パフォーマンスチェック用SS
	if (fps < 10) and (ss_object.enabled) and (not ss_complated):
		ss_object.save_to()
		ss_complated = true


func _on_player_changed(new_position: Vector2):
	player_pos.text = "Player POS: %.2f-%.2f" % [new_position.x, new_position.y]


func _on_player_ready_changed(isReady):
	if isReady:
		player = GameManager.player
		var connect_error = player.position_changed.connect(_on_player_changed)
		assert(connect_error == OK, "connect error: player signal('position_changed')")
	else:
		if player.position_changed.is_connected(_on_player_changed):
			player.position_changed.disconnect(_on_player_changed)
		player = null
