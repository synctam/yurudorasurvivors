class_name MobBatSpawner
extends Node2D

@export var max_mobs: int = 100
@export var spwan_interval: float = 1.0
# エリートモンスターの出現確率(1/elite_monstor_spwan_rate)
@export var elite_monstor_spwan_rate: int = 500

@onready var packed_bat: PackedScene = preload("res://Enemies/Bat/Mob_Bat.tscn")

@onready var spawn_timer: Timer = $SpawnTimer 
@onready var screen_size: Vector2 = get_viewport_rect().size # 画面サイズを取得
@onready var offser_x: float = screen_size.x / 2
@onready var offser_y: float = screen_size.y / 2

var mob_count: int = 0

#
# 初期化
#
func _ready():
	spawn_timer.wait_time = spwan_interval

#
# [ebent] 敵生成
#
func _on_SpawnTimer_timeout():
	if GameManager.player.is_death:
		pass
	else:
		# コウモリ
		if mob_count < max_mobs:
			var bat = packed_bat.instantiate()
			var pos_x  = GameManager.random_object.randf_range(-offser_x, offser_x)
			var pos_y  = GameManager.random_object.randf_range(-offser_y, offser_y)
			var dir = GameManager.random_object.randi_range(0, 3)
			match dir:
				0: # notth
					bat.position = Vector2(GameManager.player.position.x + pos_x, GameManager.player.position.y - offser_y)
				1: # east
					bat.position = Vector2(GameManager.player.position.x + offser_x, GameManager.player.position.y + pos_y)
				2: # south
					bat.position = Vector2(GameManager.player.position.x + pos_x, GameManager.player.position.y + offser_y)
				3: # west
					bat.position = Vector2(GameManager.player.position.x - offser_x, GameManager.player.position.y + pos_y)
			
			if GameManager.random_object.randi_range(0, elite_monstor_spwan_rate) == 0:
				# エリートモンスターをスポーン
				bat.is_elite = true
			else:
				# 通常モンスターをスポーン
				pass
			add_child(bat)
			mob_count += 1
