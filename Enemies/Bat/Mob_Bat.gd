class_name Mob_Bat
extends CharacterBody2D

#
# 設定情報
#
@export var speed: float = 50.0
@export var hit_point: float = 5.0
@export var xp_point: float = 5.0
@export var attack_damage: float = 5.0
@export var attack_interval: float = 1.0
@export var is_elite: bool = false
# エリートモンスターの hit point の倍率
@export var elite_hp_ratio: float = 10.0
# エリートモンスターの xp point の倍率
@export var elite_xp_ratio: float = 10.0

var packed_xp_jewel = preload("res://Items/Jewels/XpJewel.tscn")
# elite_monstor_material は onready は使用禁止
var elite_monstor_material = preload("res://Enemies/Bat/MobBatEliteMaterial.tres")

#
# ローカルノード
#
@onready var attack_timer: Timer = $AttackTimer
@onready var animated_sprite: AnimatedSprite2D = $AnimatedSprite2D
@onready var attack_sound = $AttackSound

#
# シグナル
#
signal dead # 死亡時に発動。xp_point を通知する。

#
# コウモリの初期化
#
func _ready():
	if is_elite:
		set_elite()

	animated_sprite.play()
	attack_timer.wait_time = attack_interval
	if GameManager.player:
		var connect_error = GameManager.player.status_changed.connect(_on_player_status_changed)
		assert(connect_error == OK, "connect error: player signal('status_changed').")


#
# [event] 物理プロセス
#
func _physics_process(_delta: float) -> void:
	if GameManager.player:
		if not GameManager.player.is_death:
			move_to_player()


#
# プレイヤーを目指して移動する
#
func move_to_player():
	var direction = (GameManager.player.position - self.position).normalized()
	set_velocity(direction * speed)
	move_and_slide()
	var _new_direction = velocity

#
# 攻撃処理
#
func Attack_to_player():
	if GameManager.player.is_death:
		pass
	else:
		GameManager.player.attacked_from_enemy(attack_damage)
		attack_sound.play()


#
# [event] プレイヤーの状態が変化した時
# - ALIVE: 正常
# - DEATH: 死亡
#
func _on_player_status_changed(status):
	if status ==  GameManager.player.Status.DEATH:
		attack_timer.stop()
		if GameManager.player.status_changed.is_connected(_on_player_status_changed):
			GameManager.player.status_changed.disconnect(_on_player_status_changed)


#
# [event] プレイヤーに接触した時、攻撃する。
# 攻撃タイマーを起動する
#
func _on_Player_body_entered(body):
	if (body is Player) and (not GameManager.player.is_death):
		attack_timer.start()
		Attack_to_player()


#
# [event] プレイヤーから離れた時、攻撃をやめる。
# 攻撃タイマーを停止する。
#
func _on_Player_body_exited(body):
	if (body is Player) and (not GameManager.player.is_death):
		attack_timer.stop()


#
# [event] 攻撃タイマーが発動した時の処理
#
func _on_Attack_Timer_timeout():
	Attack_to_player()


#
# 攻撃を受けた時
# power: 攻撃力
#
func hit(power: float) -> void:
	hit_point = max(hit_point - power, 0.0)
	if hit_point <= 0.0:
		dead.emit(xp_point)
		drop_xp_jewel()
		queue_free()
	else:
		pass


func drop_xp_jewel() -> void:
	var xp_jewel = packed_xp_jewel.instantiate()
	xp_jewel.position = self.position
	xp_jewel.xp_point = self.xp_point
	var field = get_parent().get_parent()
	# field.add_child(xp_jewel)
	# ToDo: ここで add.child() を実行すると以下のエラーが発生するため、call_deferred() に差し替えた。
	# ERROR: Can't change this state while flushing queries.
	# なぜ add.child() できないのかを調査すること
	field.add_child.call_deferred(xp_jewel)


#
# [property] エリートモンスターの有無を設定
# エリートモンスターの場合は、HP と XP が１０倍
#
func set_elite() -> void:
	$AnimatedSprite2D.material = elite_monstor_material
	hit_point = hit_point * elite_hp_ratio
	xp_point = xp_point * elite_xp_ratio
