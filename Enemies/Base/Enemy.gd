class_name Enemy
extends CharacterBody2D

@onready var speed: float = 50.0
@onready var hit_point: float = 5.0
@onready var xp_point: float = 5.0
@onready var attack_point: float = 5.0
@onready var attack_interval: float = 1.0

func _ready():
	$AnimatedSprite2D.playing = true

func _physics_process(delta: float) -> void:
	move_to_player()


#
# プレイヤーを目指して移動する
#
func move_to_player():
	var gp = global_position
	var direction = (GameManager.player.position - self.position).normalized()
	set_velocity(direction * speed)
	move_and_slide()

#
# [event] プレイヤーに接触した時、攻撃タイマーを起動する
#
func _on_Area2D_body_entered(body):
	if body is Player:
		$Timer.start()
		print("timer started!")
	pass

#
# [event] プレイヤーから離れた時、タイマーを停止させる。
#
func _on_Area2D_body_exited(body):
	if body is Player:
		$Timer.stop()
		print("timer stopped!")


#
# [event][ToDo] タイマーが発動した時、攻撃を行う。
#
func _on_Timer_timeout():
	pass
