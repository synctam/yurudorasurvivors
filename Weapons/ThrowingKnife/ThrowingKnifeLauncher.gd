#
# Throwing Knife Launcher
# 投げナイフの発射を管理する
#
class_name ThrowingKnifeLauncher
extends Node2D

# 投げナイフのロード
@onready var pacled_throwing_knife: PackedScene = preload("res://Weapons/ThrowingKnife/ThrowingKnife.tscn")

#
# Node objects
#
@onready var attack_timer: Timer = $AttackTimer

# 発射間隔
@export var attack_interval: float = 1.0: set = set_attack_interval
# 連射弾数
@export var burst_count: int = 1: set = set_burst_count
# 連射間隔
@export var burst_interval: float = 0.1: set = set_burst_interval


@onready var screen_size: Vector2 = get_viewport_rect().size # 画面サイズを取得
@onready var offser_x: float = screen_size.x / 2
@onready var offser_y: float = screen_size.y / 2


#
# 初期設定
#
func _ready():
	attack_timer.wait_time = attack_interval
	attack_timer.start()
	var connect_error = GameManager.player_ready_changed.connect(_on_player_readt_changed)
	assert(connect_error == OK, "connect error: GameManager 'player_ready_changed'")
#
# 複数の武器を生成する
#
func create_weapons():
	# 目標の位置をランダムに設定
	var pos_x  = GameManager.random_object.randf_range(-offser_x, offser_x)
	var pos_y  = GameManager.random_object.randf_range(-offser_y, offser_y)
	var dir = GameManager.random_object.randi_range(0, 3)

	var target: Vector2 = Vector2.ZERO
	match dir:
		0: # notth
			target = Vector2(GameManager.player.position.x + pos_x, GameManager.player.position.y - offser_y)
		1: # east
			target = Vector2(GameManager.player.position.x + offser_x, GameManager.player.position.y + pos_y)
		2: # south
			target = Vector2(GameManager.player.position.x + pos_x, GameManager.player.position.y + offser_y)
		3: # west
			target = Vector2(GameManager.player.position.x - offser_x, GameManager.player.position.y + pos_y)
	
	# burst_count の本数の投げナイフを生成する
	for _i in range(burst_count):
		await get_tree().create_timer(burst_interval).timeout
		create_weapon(target)


#
# 武器を生成し、目標を設定する。
#
func create_weapon(target: Vector2):
	var throwing_knife: ThrowingKnife = pacled_throwing_knife.instantiate()
	throwing_knife.position = GameManager.player.position
	get_parent().get_parent().add_child(throwing_knife)

	throwing_knife.look_at(target)
	throwing_knife.hit_to(target)

#
# [property] 攻撃間隔を設定
#
func set_attack_interval(new_value):
	if attack_timer:
		attack_timer.wait_time = new_value

	attack_interval = new_value


#
# [property] 連射弾数の設定
#
func set_burst_count(new_value: int) -> void:
	burst_count = new_value


#
# [property] 連射間隔の設定
#
func set_burst_interval(new_value: float) -> void:
	burst_interval = new_value


#
# [event] 攻撃タイマー
#
func _on_AttackTimer_timeout():
	create_weapons()

#
# [event] プレイヤーが動作可能になった時
#
func _on_player_readt_changed(new_value):
	if new_value:
		var connect_error = GameManager.player.status_changed.connect(_on_player_status_changed)
		assert(connect_error == OK, "connect error: GameManager.player 'status_changed'")
	else:
		pass


#
# [event] プレイヤーの状態が変化した時
#
func _on_player_status_changed(state: int):
	match state:
		Player.Status.IDLE:
			pass
		Player.Status.ALIVE:
			pass
		Player.Status.DEATH:
			# 死亡時は攻撃タイマーを停止する
			attack_timer.stop()
