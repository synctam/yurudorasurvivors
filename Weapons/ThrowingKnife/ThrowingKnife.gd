#
# ThrowingKnife
# 機能：最寄りの敵に向かってナイフを投げる
#
class_name ThrowingKnife
extends Area2D

# 武器のレベル
@export var level: int = 1
# 武器の最大レベル
@export var max_level: int = 5
# 攻撃力
@export var power: float = 5.0
# 速度：弾のスピード
@export var speed: float = 200.0
# 持続時間：弾の飛行時間（秒）
@export var duration_time: float = 3.0
# 敵を貫通する回数
@export var penetration_count: int = 0

#
# ローカルノード
#
@onready var duration_timer: Timer = $DurationTimer

var target: Vector2 = Vector2.ZERO

func _ready():
	duration_timer.wait_time = duration_time
	duration_timer.start()


func _physics_process(delta):
	# 攻撃目標に移動する
	position += transform.x * speed * delta

#
# 指定した位置を攻撃する
#
func hit_to(new_position: Vector2) -> void:
	target = new_position


#
# [event] mask(enemies)に弾が当たった時
#
func _on_body_entered(body: CharacterBody2D):
	# ToDo: 衝突音を鳴らす
	body.hit(power)
	if penetration_count <= 0:
		queue_free()
	else:
		penetration_count -= 1


#
# [event] 持続時間が経過した時
#
func _on_DurationTimer_timeout():
	queue_free()
