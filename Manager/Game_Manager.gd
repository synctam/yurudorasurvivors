extends Node2D

@onready var random_object:RandomNumberGenerator = RandomNumberGenerator.new()

var gold = 0
var player: Player = null

var master_volume: float = 0.0
var music_volume: float = -10.0
var sfx_volume: float = -10.0


signal player_ready_changed

#
# 初期化
#
func _ready():
	# 乱数の初期化
	GameManager.random_object.seed = hash("yurudora")
	# バスの設定
	var master_bus_index = AudioServer.get_bus_index("Master")
	AudioServer.set_bus_volume_db(master_bus_index, master_volume)
	var music_bus_index = AudioServer.get_bus_index("Music")
	AudioServer.set_bus_volume_db(music_bus_index, music_volume)
	var sfx_bus_index = AudioServer.get_bus_index("Sfx")
	AudioServer.set_bus_volume_db(sfx_bus_index, sfx_volume)


#
# プレイヤーをゲームに追加
#
func set_player(new_player: Player) -> void:
	player = new_player
	var connect_error = player.status_changed.connect(_on_player_status_changed)
	assert(connect_error == OK, "connect error: player signal('status_changed')")
	player_ready_changed.emit(true)

func _on_player_status_changed(status):
	match status:
		player.Status.ALIVE:
			pass
		player.Status.DEATH:
			if player.status_changed.is_connected(_on_player_status_changed):
				player.status_changed.disconnect(_on_player_status_changed)
			player_ready_changed.emit(false)
	print("status: %d" % [status])
